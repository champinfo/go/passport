package passport

//type Config struct {
//	Secret string `required:"true" yaml:"Secret"` //jwt token password
//	Claims PublicClaims `require:"true" yaml:"Claims"`
//}

const (
	SecretKey          = "JWT_SECRET"
	SecretDefaultValue = "defaultSecretKey"
)

type PublicClaims struct {
	Audience    string `default:"champ_info" yaml:"Audience"`
	Subject     string `default:"projects" yaml:"Subject"`
	Email       string `default:"server@championtek.com.tw" yaml:"Email"`
	PhoneNumber string `default:"0000000000" yaml:"PhoneNumber"`
	Issuer      string `default:"champ_info" yaml:"Issuer"`
	Duration    int64  `default:"10" yaml:"Duration"` //seconds
}
