package passport

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	jwtMiddle "github.com/iris-contrib/middleware/jwt"
	"github.com/kataras/iris/v12"
	log "github.com/sirupsen/logrus"
	"os"
	"time"
)

// Token type of *jwt.Token
type Token *jwt.Token

type Passport interface {
	GenerateToken(claims *PublicClaims) (Token, error)
	ValidateToken(tokenRaw string, needRefresh bool) (t Token, fresh bool, err error)
	RefreshToken(tokenRaw string) (Token, error)
	RetrieveClaimsFromToken(tokenRaw string) (*PublicClaims, error)
	RetrieveTokenFromHeader(ctx iris.Context) (tokenRaw string, err error)
}

type passport struct{}

var Psp Passport

func init() {
	Psp = &passport{}
}

func (pp *passport) GenerateToken(claims *PublicClaims) (Token, error) {
	if claims.Duration == 0 {
		return nil, errors.New("the secret or duration for generating token should not be empty or zero")
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub":          claims.Subject,
		"aud":          claims.Audience,
		"email":        claims.Email,
		"phone_number": claims.PhoneNumber,
		"iss":          claims.Issuer,
		"exp":          time.Now().Unix() + claims.Duration,
		"iat":          time.Now().Unix(),
	})
	tokenRaw, err := token.SignedString([]byte(getEnv(SecretKey, SecretDefaultValue)))
	if err != nil {
		return nil, errors.New(fmt.Sprintf("sign token string error: %#v", err))
	}
	token.Raw = tokenRaw
	return token, nil
}

func (pp *passport) ValidateToken(tokenRaw string, needRefresh bool) (t Token, fresh bool, err error) {
	t = nil
	fresh = false
	//Parse function will validate the token
	token, err := jwt.Parse(tokenRaw, func(token *jwt.Token) (i interface{}, e error) {
		return []byte(getEnv(SecretKey, SecretDefaultValue)), nil
	})
	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				err = errors.New("malformed token")
				return
			} else if ve.Errors&jwt.ValidationErrorSignatureInvalid != 0 {
				err = errors.New("signature token invalid")
				return
			} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
				if needRefresh {
					nt, _ := pp.RefreshToken(tokenRaw)
					t = nt
					fresh = true
					err = errors.New("token expired and refresh")
					return
				}
				err = errors.New("token expired and not refresh")
				return
			} else {
				err = errors.New("other validation errors")
				return
			}
		} else {
			err = errors.New("unknown error")
			return
		}
	}
	t = token
	if !t.Valid {
		err = errors.New("token is not valid")
		return
	}
	return
}

func (pp *passport) RefreshToken(tokenRaw string) (Token, error) {
	claims, err := pp.RetrieveClaimsFromToken(tokenRaw)

	if err != nil {
		return nil, errors.New(fmt.Sprintf("parse token in refresh process error: %v", err))
	}

	token, err := pp.GenerateToken(claims)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("refresh token error: %v", err))
	}
	return token, nil
}

func (pp *passport) RetrieveClaimsFromToken(tokenRaw string) (*PublicClaims, error) {
	if len(tokenRaw) == 0 {
		return nil, errors.New("can not find the token")
	}
	//需要注意不能呼叫ValidateToken，避免迴圈RefreshToken->RetrieveClaimsFromToken->ValidateToken->RefreshToken
	token, err := jwt.Parse(tokenRaw, func(token *jwt.Token) (i interface{}, e error) {
		return []byte(getEnv(SecretKey, SecretDefaultValue)), nil
	})
	//我們只要token不是ValidationErrorMalformed/ValidationErrorSignatureInvalid就可以
	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				return nil, errors.New(fmt.Sprintf("invlid token error: %v", err))
			} else if ve.Errors&jwt.ValidationErrorSignatureInvalid != 0 {
				return nil, errors.New(fmt.Sprintf("invlid token error: %v", err))
			}
		}
	}
	if token == nil {
		return nil, errors.New("token is empty")
	}

	claims := token.Claims.(jwt.MapClaims)

	var convertedExp, convertIap int64
	switch (token.Claims.(jwt.MapClaims)["exp"]).(type) {
	case int64:
		convertedExp = token.Claims.(jwt.MapClaims)["exp"].(int64)
		convertIap = token.Claims.(jwt.MapClaims)["iat"].(int64)
	case float64:
		exp := token.Claims.(jwt.MapClaims)["exp"].(float64)
		iap := token.Claims.(jwt.MapClaims)["iat"].(float64)
		convertedExp = int64(exp)
		convertIap = int64(iap)
	}

	sub, ok := claims["sub"].(string)
	if !ok {
		sub = ""
	}
	aud, ok := claims["aud"].(string)
	if !ok {
		aud = ""
	}
	email, ok := claims["email"].(string)
	if !ok {
		email = ""
	}
	phone, ok := claims["phone_number"].(string)
	if !ok {
		phone = ""
	}
	iss, ok := claims["iss"].(string)
	if !ok {
		iss = ""
	}

	return &PublicClaims{
		Subject:     sub,
		Audience:    aud,
		Email:       email,
		PhoneNumber: phone,
		Issuer:      iss,
		Duration:    convertedExp - convertIap,
	}, nil
}

func (pp *passport) RetrieveTokenFromHeader(ctx iris.Context) (tokenRaw string, err error) {
	return jwtMiddle.FromAuthHeader(ctx)
}

// GetEnv os.GetEnv helper
func getEnv(key, fallback string) string {
	s := os.Getenv(key)
	log.Debugln(s)
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
