package tests

import (
	"fmt"
	"git.championtek.com.tw/go/passport"
	"testing"
	"time"
)

func TestGenerateToken(t *testing.T) {
	claims := passport.PublicClaims{
		Audience:    "UnitTest",
		Subject:     "",
		Email:       "test@example.com",
		PhoneNumber: "0988808999",
		Issuer:      "",
		Duration:    10,
	}

	token, err := passport.Psp.GenerateToken(&claims)
	if err != nil {
		t.Errorf("Generate token has error: %v", err)
	}

	if token == nil {
		t.Errorf("Generate token, got: %v, it should not be empty.", token)
	}
	fmt.Println(token.Raw)
}

func TestValidToken(t *testing.T) {
	claims := passport.PublicClaims{
		Audience:    "UnitTest",
		Subject:     "",
		Email:       "test@example.com",
		PhoneNumber: "0988808999",
		Issuer:      "",
		Duration:    10,
	}

	token, err := passport.Psp.GenerateToken(&claims)
	if err != nil {
		t.Errorf("Generate token has error: %v", err)
	}
	if token == nil {
		t.Errorf("Generate token, got: %v, it should not be empty.", token)
	}

	vt, _, err := passport.Psp.ValidateToken(token.Raw, false)
	if vt == nil && err != nil {
		t.Errorf("Validate token failed and err %v", err)
	}
}

func TestRefreshToken(t *testing.T) {
	claims := passport.PublicClaims{
		Audience:    "UnitTest",
		Subject:     "",
		Email:       "test@example.com",
		PhoneNumber: "0988808999",
		Issuer:      "",
		Duration:    10,
	}

	token, err := passport.Psp.GenerateToken(&claims)
	if err != nil {
		t.Errorf("Generate token has error: %v", err)
	}
	if token == nil {
		t.Errorf("Generate token, got: %v, it should not be empty.", token)
	}

	time.Sleep(11 * time.Second)

	vt, fresh, err := passport.Psp.ValidateToken(token.Raw, true)
	if (vt == nil || fresh == false) && err != nil {
		t.Errorf("Validate token failed and err %v", err)
	}
	fmt.Println(vt.Raw)
}

func TestRetrieveClaimsFromToken(t *testing.T) {
	claims := passport.PublicClaims{
		Audience:    "UnitTest",
		Subject:     "",
		Email:       "test@example.com",
		PhoneNumber: "0988808999",
		Issuer:      "",
		Duration:    10,
	}

	token, err := passport.Psp.GenerateToken(&claims)
	if err != nil {
		t.Errorf("Generate token has error: %v", err)
	}
	if token == nil {
		t.Errorf("Generate token, got: %v, it should not be empty.", token)
	}

	cfg, err := passport.Psp.RetrieveClaimsFromToken(token.Raw)

	if err != nil {
		t.Errorf("Retrieve token claims error: %v", err)
	}

	if cfg.Audience != "UnitTest" || cfg.Subject != "" || cfg.Email != "test@example.com" || cfg.PhoneNumber != "0988808999" || cfg.Issuer != "" {
		t.Errorf("Retrieve token claims error: %v", cfg)
	}
}
